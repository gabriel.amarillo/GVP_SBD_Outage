function obtenerAudiosNivelOutage(outages) {

// ARMO LA FRASE.
var ruta = "../Resources/Prompts/";
if (isArray(outages))
{
	if (outages[0].TIME != undefined)
	{
	 return  [{"type":"audio","value":ruta + "masivo.wav"},{"type":"audio","value":ruta + "tiempo.wav"},{"type":"time","value":outages[0].TIME}]
	}
	else
	{
	   return undefined;	 
	}	


}
else{
	

	if (outages.TIME != undefined)
	{
	 return  [{"type":"audio","value":ruta + "masivo.wav"},{"type":"audio","value":ruta + "tiempo.wav"},{"type":"time","value":outages.TIME}]
	}
	else
	{
	   return undefined;	 
	}

}
	
}

function compare(a, b) {
	// Use toUpperCase() to ignore character casing
	const ponderacionA = a.ponderacion;
	const ponderacionB = b.ponderacion;
	
	var comparison = 0;
	if (ponderacionA > ponderacionB) {
		comparison = 1;
	} else if (ponderacionA < ponderacionB) {
		comparison = -1;
	}
	return comparison;
}

/**
 * agrego niveles de outages.
 * @param niveles
 */
function agregarPonderacion(niveles){
var miArray = []
miArray = niveles;


	for (var i = 0 ; i < miArray.length;i++) {
		miArray[i].ponderacion = addPonderaciones(miArray[i]);		
	}
	return 	niveles;	
}

function addPonderaciones(outage){
	var ponderacion = {"OUT_ZONAL":"1","OUT_SS":"2", 
			"OUT_DECO_1":"3","OUT_DECO_2":"4","OUT_REVERSE":"5",
			"OUT_POP3":"6","OUT_WEBMAIL":"7"}
	
	for (var prop in ponderacion) {
		if (ponderacion.hasOwnProperty(outage)) {
			return ponderacion[outage]
		}
	}

	return undefined;

}

		
/**
* genero la hora y minutos para el outage
* @fechaWS = fecha que responde el WSgetinfocliet 
*/
function restarFechas(fechaWS){
	//en milisegundos
	var currMili = new Date().getTime();
	var wsMili = new Date(fechaWS).getTime();
	
	
	if ( currMili < wsMili) {  // fecha y hora aun vigente
		var difMili = wsMili - currMili;				
		var hora = difMili/3600000;
		var restohora = difMili%3600000;
		var minuto = restohora/60000;
		
		return {"hora":hora,"minuto":minuto};
	} 
	 
return undefined;	
}



function outagesDentroDeUmbral(Outages,Horas){
	var Array =[];
	var today = new Date();
		for (var i = 0 ; i < Outages.length;i++) {
	       var hours = Math.abs(today - Outages[i].fecha) / (60*60*1000);

		    if (hours <= Horas)
	          {
	          
	          Array.push(Outages[i])
	          }
	         
		}
		return 	Array;	
}



function agregarDatos(Outages){

	for (var i = 0 ; i < Outages.length;i++) {
			Outages[i].ponderacion = ponderaciones(Outages[i].CODE);	
	  		Outages[i].fecha =creaFecha(Outages[i].DATE,Outages[i].TIME)
	}
return 	Outages;	
}

function ponderaciones(strOutage){
		var ponderacion = {"OUT_ZONAL":"1","OUT_SS":"2",
				"OUT_DECO_1":"3","OUT_DECO_2":"4","OUT_REVERSE":"5",
				"OUT_POP3":"6","OUT_WEBMAIL":"7"}
		for (var prop in ponderacion) {
			if (ponderacion.hasOwnProperty(strOutage)) {
				return ponderacion[strOutage]
			}
		}
return undefined;
}

function creaFecha(date2, time2){
 var d = date2.split("-");
 var t = time2.split(":");
 var dd= new Date(d[0],(d[1]-1),d[2],t[0],t[1],0,0);
	return dd;
}

function compareFecha(a, b) {
	  // Use toUpperCase() to ignore character casing
 
  const fechaA =a.fecha;
  const fechaB= b.fecha;
  var comparison = 0;
  if (fechaA > fechaB) {
	    comparison = -1;
  } else if (fechaA < fechaB ) {
	    comparison = 1;
  }
 
return comparison;
}
	
	
function comparePonderacion(a, b) {
	  // Use toUpperCase() to ignore character casing
	 
  const ponderacionA =a.ponderacion;
  const ponderacionB= b.ponderacion;
  var comparison = 0;
	  if (ponderacionA > ponderacionB) {
	    comparison = -1;
	  } else if (ponderacionA < ponderacionB ) {
	    comparison = 1;
	  }
	  
  return comparison;
}


